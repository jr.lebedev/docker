SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));

GRANT ALL PRIVILEGES ON bap_standard.* TO app_user@'%';

CREATE DATABASE bap_dev_test;

GRANT ALL PRIVILEGES ON bap_dev_test.* TO app_user@'%';