FROM php:7.1.12-fpm

WORKDIR /app

ARG DEBIAN_FRONTEND=noninteractive
ARG USER
ARG UID

RUN useradd -l --shell /bin/bash -u $UID -o -c "" -m $USER

RUN apt-get update && apt-get install -y apt-utils

RUN apt-get update && apt-get install -y \
    wget \
    libxrender1 \
    libfontconfig1 \
    libx11-dev \
    libjpeg62 \
    libxtst6 \
    && wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz -O /usr/local/bin/wkhtmltopdf \
    && chmod +x /usr/local/bin/wkhtmltopdf

RUN apt-get update && apt-get install -y \
    vim \
    libicu-dev \
    apt-transport-https \
    zip \
    libpng-dev \
    libmcrypt-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    git \
    libxml++2.6-dev \
    tidy

RUN docker-php-ext-install mbstring \
    zip \
    intl \
    pdo \
    pdo_mysql \
    iconv \
    mcrypt \
    soap \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

RUN yes | pecl install apcu xdebug-2.5.5 igbinary redis-3.1.6

RUN rm -rf /var/lib/apt/lists/*

CMD ["php-fpm"]
