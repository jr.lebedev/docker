version: "3.3"

services:
  nginx:
    build:
      context: ./nginx
      dockerfile: Dockerfile
      args:
        UID: ${UID}
        GID: ${GID}
        USER: deploy
    image: mbccp-image-nginx:0.0.1
    container_name: mbccp-nginx
    ports:
      - "80:80"
      - "443:443"
    depends_on:
      - php
    volumes:
      - ./app:/app:rw
      - ./nginx/config/mbccp.p.conf:/etc/nginx/conf.d/mbccp.p.conf:ro
      - ./nginx/config/nginx.conf:/etc/nginx/nginx.conf:rw
    secrets:
      - ssl_certificate
      - ssl_key
    networks:
      - backend
  php:
    build:
      context: ./php
      dockerfile: Dockerfile
      args:
        UID: ${UID}
        GID: ${GID}
        USER: deploy
    image: mbccp-image-php:0.0.1
    container_name: mbccp-php
    depends_on:
      - mysql-proxy
      - elastic
    volumes:
      - ./app:/app:rw
      - ./php/config/www.conf:/usr/local/etc/php-fpm.d/www.conf:ro
      - ./php/config/97-xdebug.ini:/usr/local/etc/php/conf.d/97-xdebug.ini:ro
      - ./php/config/98-apcu.ini:/usr/local/etc/php/conf.d/98-apcu.ini:ro
      - ./php/config/99-custom.ini:/usr/local/etc/php/conf.d/99-custom.ini:ro
    environment:
      XDEBUG_REMOTE_HOST: ${XDEBUG_REMOTE_HOST}
      XDEBUG_IDE_KEY: ${XDEBUG_IDE_KEY}
      XDEBUG_PORT: ${XDEBUG_PORT}
    networks:
      - backend
  php-cli:
    build:
      context: ./php-cli
      dockerfile: Dockerfile
      args:
        UID: ${UID}
        GID: ${GID}
        USER: ${USER}
    image: mbccp-image-php-cli:0.0.1
    container_name: mbccp-php-cli
    depends_on:
      - mysql-proxy
    volumes:
      - ./app:/app:rw
      - ./php/config/98-apcu.ini:/usr/local/etc/php/conf.d/98-apcu.ini:ro
      - ./php/config/99-custom-cli.ini:/usr/local/etc/php/conf.d/99-custom-cli.ini:ro
      - ./php/config/97-xdebug.ini:/usr/local/etc/php/conf.d/97-xdebug.ini:ro
    environment:
      XDEBUG_REMOTE_HOST: ${XDEBUG_REMOTE_HOST}
      XDEBUG_IDE_KEY: ${XDEBUG_IDE_KEY}
      XDEBUG_PORT: ${XDEBUG_PORT}
    networks:
      - backend
  mysql-ssl:
    build: ./mysql-ssl
    image: mbccp-image-mysql-ssl:0.0.1
    container_name: mbccp-mysql-ssl
    volumes:
      - ./mysql-ssl/database:/var/lib/mysql:rw
      - ./mysql-ssl/config/mysql-prod.cnf:/etc/mysql/conf.d/mysql.cnf:ro
      - ./mysql-ssl/config/setup-prod.sql:/docker-entrypoint-initdb.d/setup-prod.sql:ro
    environment:
      TZ: ${TIME_ZONE}
      MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}
      MYSQL_DATABASE: ${MYSQL_DATABASE}
      MYSQL_USER: ${MYSQL_USER}
      MYSQL_PASSWORD: ${MYSQL_PASSWORD}
    secrets:
      - mysql-ca
      - mysql-server-cert
      - mysql-server-key
      - mysql-client-cert
      - mysql-client-key
    networks:
      - backend
  mysql-proxy:
    build: ./mysql-proxy
    image: mbccp-image-mysql-proxy:0.0.1
    container_name: mbccp-mysql-proxy
    depends_on:
      - mysql-ssl
    volumes:
      - ./mysql-proxy/data:/var/lib/proxysql:rw
      - ./mysql-proxy/config/proxysql.cnf:/etc/proxysql.cnf:ro
    environment:
      CLUSTER_NAME: cluster_name
      DISCOVERY_SERVICE: 10.20.2.4:2379
      MYSQL_DATABASE: ${MYSQL_DATABASE}
      MYSQL_USER: ${MYSQL_USER}
    secrets:
      - mysql-ca
      - mysql-client-cert
      - mysql-client-key
    networks:
      - backend
  node:
    build:
      context: ./node
      dockerfile: Dockerfile
      args:
        UID: ${UID}
        GID: ${GID}
        USER: deploy
    image: mbccp-image-node:0.0.1
    container_name: mbccp-node
    networks:
      - backend
    volumes:
      - ./app:/app:rw
  elastic:
    build: ./elastic
    image: mbccp-image-elastic:0.0.1
    container_name: mbccp-elastic
    depends_on:
      - mysql-proxy
    networks:
      - backend
  supervisor:
    build:
      context: ./supervisor
      dockerfile: Dockerfile
      args:
        UID: ${UID}
        GID: ${GID}
        USER: deploy
    image: mbccp-image-supervisor:0.0.1
    container_name: mbccp-supervisor
    depends_on:
      - mysql-proxy
    volumes:
      - ./supervisor/config/clank-server.conf:/etc/supervisor/conf.d/clank-server.conf:ro
      - ./supervisor/config/message-queue.conf:/etc/supervisor/conf.d/message-queue.conf:ro
      - ./supervisor/config/supervisor.conf:/etc/supervisor/supervisor.conf:ro
      - ./php/config/99-custom-cli.ini:/usr/local/etc/php/conf.d/99-custom-cli.ini:ro
      - ./app:/app:rw
    networks:
      - backend
secrets:
  ssl_certificate:
    file: ./nginx/ssl/certificate.pem
  ssl_key:
    file: ./nginx/ssl/key.pem
  mysql-ca:
    file: ./mysql-ssl/ssl/ca.pem
  mysql-server-cert:
    file: ./mysql-ssl/ssl/server-cert.pem
  mysql-server-key:
    file: ./mysql-ssl/ssl/server-key.pem
  mysql-client-cert:
    file: ./mysql-ssl/ssl/client-cert.pem
  mysql-client-key:
    file: ./mysql-ssl/ssl/client-key.pem
networks:
  backend:
