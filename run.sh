#!/usr/bin/env bash

if [ "$#" -lt 3 ]; then
    echo "Params required to run command in a container: [service name] [env] [command], e.g.: sh run.sh php-cli dev app/console cache:clear"
    exit 0
fi

SERVICE="$1"; shift
ENV="$1"; shift
COMMAND="$@";
export UID=$(id -u)
export GID=$(id -g)

docker-compose -f docker-compose-${ENV}.yml run --rm --no-deps -u $(id -u):$(id -g) ${SERVICE} ${COMMAND}
