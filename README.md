# README #

## Stable on ##
* ubuntu 16.04 LTS
* docker version 17.09.0-ce, build afdb6d4 
    * after installing docker run `sudo usermod -a -G docker $USER`
    * re-login(in system) after this command
* docker-compose version 1.15.0, build e12f3b9

## How to get project up and running ##
* mkdir mbccp && cd mbccp
* git clone https://gitlab.com/jr.lebedev/docker.git .
* git clone -b develop --single-branch https://p2.lebedev@git.itransition.com/scm/mbccp/mbccp.git app
* sh mbccp-build.sh dev
* sudo echo 127.0.0.1 mbccp.l > /etc/hosts && sudo echo 127.0.0.1 mbccp.p > /etc/hosts

## Help scripts ##
* run.sh [service name] [env] [command to execute in container]
this command helps to connect into container and execute a command e.g. `sh run.sh php-cli composer analyze`

## Misc ##
* Important service host must be defined as its service name defined in docker-compose file, cos container's IP is dynamic
