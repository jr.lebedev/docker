#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "You should pass [env] variable to script, e.g.: sh mbccp-build.sh dev"
    exit 0
fi

ENV="$1"

echo '---> Variables'
export UID=$(id -u)
export GID=$(id -g)
echo '---> Cleaning'
sed -i -e "s/installed:.*/installed:\ false/" app/app/config/parameters.yml
rm -rf app/app/cache/dev && rm -rf app/app/cache/sessions
echo '---> Installing dependencies'
sh run.sh php-cli ${ENV} composer install --no-interaction
echo '---> Copy certificates and keys'
cp ./certificates/nginx/* ./nginx/ssl/
cp ./certificates/mysql/* ./mysql-ssl/ssl/
cp ./certificates/parameters.yml ./app/app/config/
cp ./certificates/parameters_test.yml ./app/app/config/
echo '---> Rise and shine'
docker-compose -f docker-compose-${ENV}.yml up -d
echo '---> Installing ORO'
sh run.sh php-cli ${ENV} app/console oro:install --drop-database --application-url=mercedes.dev --organization-name=MBCCP --sample-data=y  --user-name=admin --user-email=admin@example.com --user-password=12345678 --user-firstname=John --user-lastname=Smith --env=dev --timeout=0 -vvv
echo '---> Building front application'
sh run.sh node ${ENV} yarn install
sh run.sh node ${ENV} yarn build:${ENV}
sh run.sh php-cli ${ENV} app/console assetic:dump
echo '---> Warming up cache'
sh run.sh php-cli ${ENV} app/console cache:clear --env=${ENV}
sh run.sh php-cli ${ENV} app/console fos:elastica:populate
